## IMPORT/EXPORT BOOKS REST API

### Challenge
Implement a REST API using Express.js that handles Export and Import requests. The solution should ideally be written in Typescript, or else using plain JavaScript's `class` structure.

The API should expose endpoints to:
- `POST` a request for a **new Export job**. Valid requests should be saved in memory. Invalid requests should return an error. The request must have the following schema:

  ```javascript
  {
    bookId: string,
    type: "epub" | "pdf"
  }
  ```

- `GET` a list of **Export requests**, grouped by their current `state` (see below).
- `POST` a request for a new **Import job**. Valid requests should be saved in memory. Invalid requests should return an error. The request must have the following schema:

  ```javascript
  {
    bookId: string,
    type: "word" | "pdf" | "wattpad" | "evernote",
    url: string
  }
  ```

- `GET` a list of **Import requests**, grouped by their current `state` (see below).

Both export and import requests should be created with a `pending` state, and with a `created_at` timestamp. An import or export should take the amount of time outlined below. After the specified time, the state should be updated from `pending` to `finished` and update an `updated_at` timestamp.

| Job type     | Processing time (s) |
| ------------ | ------------------- |
| ePub export  | 10                  |
| PDF export   | 25                  |
| import (any) | 60                  |

Add test coverage as you see fit.

The project should be responsible for managing all the required dependencies and should run just by using `npm install` and `npm start`.

### Instructions
`npm install`  
`npm start`  

`npm serve` - to pretty-print logs  
`npm test` - to test  

If you are using VS Code and have [Rest Client Extension](https://marketplace.visualstudio.com/items?itemName=humao.rest-client) open `http.rest` file.

You can use curls:

```bash
curl --request POST \
  --url http://localhost:3000/api/v1/books/exports \
  --header 'content-type: application/json' \
  --data '{"bookId": "ac2d64a0-c44e-11e9-b933-559a4f9d4e90","type": "pdf"}'
```

```bash
curl --request POST \
  --url http://localhost:3000/api/v1/books/exports \
  --header 'content-type: application/json' \
  --data '{"bookId": "ac2d64a0-c44e-11e9-b933-559a4f9d4e90","type": "epub"}'
```

```bash
curl --request GET \
  --url http://localhost:3000/api/v1/books/exports
```

```bash
curl --request POST \
  --url http://localhost:3000/api/v1/books/imports \
  --header 'content-type: application/json' \
  --data '{"bookId": "ac2d64a0-c44e-11e9-b933-559a4f9d4e9","type": "pdf","url": "https://www.adobe.com/be_en/active-use/pdf/Alice_in_Wonderland.pdf"}'
```

```bash
curl --request GET \
  --url http://localhost:3000/api/v1/books/imports
```

Server loggers will log all requests/responses and tasks.