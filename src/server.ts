import { systemLogger } from './loggers'
import app from './app'
import { defaultTimeoutToShutdown } from './constants'
import { Server } from 'http'

let server: Server
async function start () {
  try {
    server = app.listen(
      process.env.SERVER_PORT, () => systemLogger.info(`server started at port ${process.env.SERVER_PORT}`)
    )
    systemLogger.info('App started')
  } catch (e) {
    systemLogger.error(e.toString(), e.stack)
  }
}

start().catch((e: any) => console.error('Unhandled error', { e }))

process.on('SIGINT', async () => {
  systemLogger.info('SIGINT: shutting down gracefully')
  setTimeout(() => {
    systemLogger.info('SIGINT: forcing shut down')
    process.exit(1)
  }, defaultTimeoutToShutdown)
  server.close(async () => {
    systemLogger.info('SIGINT: connections closed')
    process.exit(0)
  })
})

process.on('uncaughtException', (e) => { if (e) { systemLogger.error(e) } })
process.on('unhandledRejection', (e) => { if (e) { systemLogger.error(e) } })
