import Queue from '../lib/TaskQueue'
const exportQueue = new Queue('ImportBooksQueue')
import { ImportBookType, TaskProcessingTime } from '../types'

function createImportTask () {
  return () => new Promise(resolve => setTimeout(resolve, TaskProcessingTime.import))
}

const importBook = (bookId: string, type: ImportBookType, url: string) => {
  const task = exportQueue.addTask(createImportTask(), { bookId, type, url })

  return task
}

const getImportTasks = () => { return exportQueue.getTasks() }

export {
  importBook,
  getImportTasks
}
