import Queue from '../lib/TaskQueue'
const exportQueue = new Queue('ExportBooksQueue')
import { ExportBookType, TaskProcessingTime } from '../types'

function createExportTask (type: ExportBookType) {
  const processingTime =
    type === ExportBookType.epub ? TaskProcessingTime.epub : TaskProcessingTime.pdf

  return () => new Promise(resolve => setTimeout(resolve, processingTime))
}

const exportBook = (bookId: string, type: ExportBookType) => {
  const t = createExportTask(type)
  const task = exportQueue.addTask(t, { bookId, type })

  return task
}

const getExportsTasks = () => { return exportQueue.getTasks() }

export {
  exportBook,
  getExportsTasks
}
