import httpStatus from 'http-status-codes'

export interface CustomError extends Error {
  status: number,
  error: object
}

class InternalError extends Error {
  status: number
  error: object
  constructor (error = {}, message = 'Internal error') {
    super(message)
    this.name = this.constructor.name
    this.status = httpStatus.INTERNAL_SERVER_ERROR
    this.error = error
    Error.captureStackTrace(this, this.constructor)
  }
}

class InvalidFieldError extends Error {
  status: number
  error: object
  constructor (error: object, message = 'InvalidFieldError') {
    super(message)
    this.name = this.constructor.name
    this.status = httpStatus.BAD_REQUEST
    this.error = error
    Error.captureStackTrace(this, this.constructor)
  }
}

export {
  InternalError,
  InvalidFieldError
}
