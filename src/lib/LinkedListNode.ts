export default class LinkedListNode<T> {
  public value: T
  public next: LinkedListNode<T> | null

  constructor (value: T, next: (LinkedListNode<T> | null) = null) {
    this.value = value
    this.next = next
  }

  public toString (cb?: Function) {
    return cb ? cb(this.value) : `${this.value}`
  }
}
