import Queue from './Queue'
import { makeLogger } from '../loggers'
import { Logger } from 'pino'
import uuid from 'uuid/v1'
import { Task, TaskState } from '../types'

interface TasksLookup {
  [id: string]: Task
}

export default class TaskQueue extends Queue {
  public readonly pendingTasks: TasksLookup = {}
  public readonly finishedTasks: TasksLookup = {}
  private readonly logger: Logger

  constructor (name: string, concurrency?: number) {
    super(name, concurrency)
    this.logger = makeLogger(name)
  }

  private finishTask (taskId: string) {
    const pendingTask = this.pendingTasks[taskId]
    pendingTask.state = TaskState.finished
    pendingTask.updated_at = Date.now()
    pendingTask.processingTime = pendingTask.updated_at - pendingTask.created_at

    this.logger.info({ pendingTask }, 'Task is completed')
    this.finishedTasks[taskId] = pendingTask
    delete this.pendingTasks[taskId]
  }

  public addTask<T> (job: Function, description: T = {} as T): Task & T {
    const task: Task & T = Object.assign({
      id: uuid(),
      state: TaskState.pending,
      created_at: Date.now()
    }, description)

    const j = () => {
      return new Promise(async (resolve) => {
        this.pendingTasks[task.id] = task
        this.logger.info({ task }, 'Starting to execute a task')
        await job()
        this.finishTask(task.id)
        resolve()
      }).catch((err) => { this.logger.error(err) })
    }

    this.addJob(j)
    return task
  }

  public getTasks () {
    return { pending: this.pendingTasks, finished: this.finishedTasks }
  }
}
