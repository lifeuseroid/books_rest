import { defaultConcurrency } from '../constants'

export default class Queue {
  public readonly name: string
  public readonly concurrency: number

  private running: number
  private readonly jobs: Function[]

  constructor (name: string, concurrency: number = defaultConcurrency) {
    this.name = name
    this.concurrency = concurrency
    this.jobs = []
    this.running = 0
  }

  public addJob (job: Function): void {
    this.jobs.push(job)
    this.next()
  }

  private next (): void {
    while (this.running < this.concurrency && this.jobs.length) {
      const job = this.jobs.shift()
      if (job) {
        job().then(() => {
          this.running--
          this.next()
        })
        this.running++
      }
    }
  }
}
