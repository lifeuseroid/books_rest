import LinkedListNode from '../LinkedListNode'
import LinkedList from '../LinkedList'
interface LuckyObject { id: number }

const luckyNumber = 7
const luckyString = 'its an honor to be here'
const luckyObject1: LuckyObject = { id : 1 }
const luckyObject2: LuckyObject = { id : 2 }
const luckyObject3: LuckyObject = { id : 3 }

describe('LinkedList', () => {
  it('should create a new linked list', () => {
    const list = new LinkedList(luckyNumber)
    expect(list).toBeInstanceOf(LinkedList)

    expect(list.peekHead()).toBe(luckyNumber)
    expect(list.peekTail()).toBe(luckyNumber)
  })

  it('should append a node to linked list', () => {
    const list = new LinkedList(luckyObject1)
    list.append(luckyObject2)

    expect(list.peekHead()).toEqual(luckyObject1)
    expect(list.peekTail()).toEqual(luckyObject2)
  })

  it('should delete а head of a linked list and return its value', () => {
    const list = new LinkedList(luckyObject1)
    list.append(luckyObject2)
    list.append(luckyObject3)

    const detachedHeadValue1 = list.deleteHead()
    expect(detachedHeadValue1).toEqual(luckyObject1)
    expect(list.peekHead()).toEqual(luckyObject2)
    expect(list.peekTail()).toEqual(luckyObject3)

    // expecting head and tail to be the same
    const detachedHeadValue2 = list.deleteHead()
    expect(detachedHeadValue2).toEqual(luckyObject2)
    expect(list.peekHead()).toEqual(luckyObject3)
    expect(list.peekTail()).toEqual(luckyObject3)

  })

  it('should delete а last linked list node and leave it empty', () => {
    const list = new LinkedList(luckyString)
    const detachedHeadValue = list.deleteHead()
    expect(detachedHeadValue).toEqual(luckyString)

    expect(list.peekHead()).toBeNull()
    expect(list.peekTail()).toBeNull()
  })

  it('should append ok to an emprty list', () => {
    const list = new LinkedList(luckyString)
    const detachedHeadValue = list.deleteHead()

    list.append(luckyString)

    expect(list.peekHead()).toEqual(luckyString)
    expect(list.peekTail()).toEqual(luckyString)
  })
})
