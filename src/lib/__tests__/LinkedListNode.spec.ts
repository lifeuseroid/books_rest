import LinkedListNode from '../LinkedListNode'
const luckyNumber: number = 7
const luckyString: string = 'luckyString'
const luckyObject =
  { name: 'lucky linkedListNode' } as object & { name: string }

describe('LinkedListNode', () => {
  it('should create linkedList with a number', () => {
    const node = new LinkedListNode(luckyNumber)

    expect(node.value).toBe(luckyNumber)
    expect(node.next).toBe(null)
  })

  it('should create linkedList with an object ', () => {
    const node = new LinkedListNode(luckyObject)

    expect(node.value.name).toBe(luckyObject.name)
    expect(node.next).toBe(null)
  })

  it('should link nodes properly', done => {
    const headNodeValue: number = 1
    const tailNodeValue: number = 2

    const tailNode = new LinkedListNode(tailNodeValue)
    const headNode = new LinkedListNode(headNodeValue, tailNode)

    expect(headNode.value).toBe(headNodeValue)
    expect(headNode.next).toBeDefined()

    if (!headNode.next) return done.fail('to prevent TS2531 error')
    expect(headNode.next.value).toBe(tailNodeValue)

    expect(tailNode.next).toBeNull()
    expect(tailNode.value).toBe(tailNodeValue)

    done()
  })

  it('should stringify node', () => {
    const nodeWithNumber = new LinkedListNode(luckyNumber)
    const nodeWithString = new LinkedListNode(luckyString)
    const nodeWithObject = new LinkedListNode(luckyObject)

    expect(nodeWithNumber.toString()).toBe(luckyNumber.toString())
    expect(nodeWithString.toString()).toBe(luckyString)
    expect(nodeWithObject.toString()).toBe('[object Object]')
  })

  it('should stringify node with custom stringifier', () => {
    const nodeWithObject = new LinkedListNode(luckyObject)
    const stringifyName =
      (value: object & { name: string }) => `name: ${value.name}`

    expect(nodeWithObject.toString(stringifyName))
      .toBe(`name: ${luckyObject.name}`)
  })

})
