import TaskQueue from '../TaskQueue'
import { TaskState } from '../../types'
import { defaultConcurrency } from '../../constants'

let jobIsFinished: number = 0

const job = () => {
  return new Promise((resolve) => {
    setTimeout(() => { jobIsFinished = 1; resolve() }, 1)
  })
}

describe('TaskQueue', () => {

  it('should create a taskQueue', () => {
    const taskQueue = new TaskQueue('testTaskQueue')
    expect(taskQueue).toBeInstanceOf(TaskQueue)
    expect(taskQueue.name).toBe('testTaskQueue')
    expect(taskQueue.concurrency).toBe(defaultConcurrency)
  })

  it('should create a taskQueue with concurrency', () => {
    const taskQueue = new TaskQueue('testTaskQueue', 10)
    expect(taskQueue.concurrency).toBe(10)
  })

  it('should create task with a job and run it', done => {
    const taskQueue = new TaskQueue('testTaskQueue')
    const task = taskQueue.addTask(job, { superId: 7 })

    expect(typeof task.id).toBe('string')
    expect(typeof task.created_at).toBe('number')
    expect(task.state).toBe(TaskState.pending)
    expect(task.superId).toBe(7)

    expect(taskQueue.pendingTasks[task.id]).toBeDefined()

    setTimeout(() => {
      expect(jobIsFinished).toBe(1)
      expect(taskQueue.finishedTasks[task.id]).toBeDefined()
      expect(taskQueue.finishedTasks[task.id].id).toBe(task.id)
      expect(taskQueue.finishedTasks[task.id].state).toBe(TaskState.finished)
      done()
    }, 1)
  })
})
