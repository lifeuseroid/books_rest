import Queue from '../queue'
import { defaultConcurrency } from '../../constants'

let jobIsFinished: number = 0

const job = () => {
  return new Promise((resolve) => {
    setTimeout(() => { jobIsFinished = 1; resolve() }, 1)
  })
}

describe('Queue', () => {

  it('should create a queue with default concurrency', () => {
    const queue = new Queue('testQueue')

    expect(queue).toBeInstanceOf(Queue)
    expect(queue.name).toBe('testQueue')
    expect(queue.concurrency).toBe(defaultConcurrency)
  })

  it('should create queue with concurrency', () => {
    const queue = new Queue('testQueue', 10)
    expect(queue.concurrency).toBe(10)
  })

  it('should add a job to queue and run it', done => {
    const queue = new Queue('testQueue')
    queue.addJob(job)
    setTimeout(() => { expect(jobIsFinished).toBe(1); done() }, 1)
  })

})
