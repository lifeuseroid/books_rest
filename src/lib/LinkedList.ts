import LinkedListNode from './LinkedListNode'
import cloneDeep from 'lodash.clonedeep'

export default class LinkedList<T> {
  private head: LinkedListNode<T> | null
  private tail: LinkedListNode<T> | null

  constructor (value: T) {
    this.head = new LinkedListNode(value)

    this.tail = this.head
  }

  peekHead = () => (this.head ? cloneDeep(this.head.value) : null)
  peekTail = () => (this.tail ? cloneDeep(this.tail.value) : null)

  append (value: T) {
    const newTailNode = new LinkedListNode(value)

    if (!this.head || !this.tail) {
      this.head = newTailNode
      this.tail = newTailNode
      return this
    }

    this.tail.next = newTailNode
    this.tail = newTailNode

    return this
  }

  deleteHead () {
    if (!this.head || !this.tail) return null

    const deleted = this.head

    if (this.head.next) {
      this.head = this.head.next
      if (!this.head.next) this.tail = this.head
    } else {
      this.head = null
      this.tail = null
    }

    return deleted.value
  }
}
