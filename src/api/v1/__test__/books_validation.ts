import Ajv from 'ajv'
const ajv = Ajv({  allErrors: true })

import { ImportBookType, ExportBookType, TaskState } from '../../../types'

enum TaskType {
  import= 'import',
  export= 'export'
}

const createValidators = (taskType: TaskType) => {
  const taskValidatorSchema = {
    type: 'object',
    properties: {
      id: { type: 'string' },
      state: { type: 'string', enum: Object.values(TaskState) },
      created_at: { type: 'number' },
      bookId: { type: 'string' },
      type: {
        type: 'string',
        enum: (taskType === TaskType.export) ?
          Object.values(ExportBookType) : Object.values(ImportBookType)
      }
    }
  }

  if (taskType === TaskType.import) Object.assign(taskValidatorSchema.properties, { url: { type: 'string' } })
  Object.assign(taskValidatorSchema, { required: Object.keys(taskValidatorSchema.properties) })

  const postResponseValidator = ajv.compile({
    type: 'object',
    properties: {
      [taskType]: taskValidatorSchema
    },
    required: [taskType]
  })

  const getResponseValidator = ajv.compile({
    type: 'object',
    properties: {
      finished: {
        type: 'object',
        patternProperties: {
          '^.*$': taskValidatorSchema
        }
      },
      pending: {
        type: 'object',
        patternProperties: {
          '^.*$': taskValidatorSchema
        }
      }
    },
    required: [TaskState.pending, TaskState.finished]
  })

  return { postResponseValidator, getResponseValidator }
}

export const {
  postResponseValidator: newExportResponseValidator,
  getResponseValidator: getExportsResponseValidator
} = createValidators(TaskType.export)

export const {
  postResponseValidator: newImportResponseValidator,
  getResponseValidator: getImportsResponseValidator
} = createValidators(TaskType.import)
