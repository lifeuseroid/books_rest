import request from 'supertest'
import bodyParser from 'body-parser'
import booksAPI from '../books'
import express from 'express'

import { ImportBookType, ExportBookType, TaskState } from '../../../types'
import {
  newExportResponseValidator, getExportsResponseValidator,
  newImportResponseValidator, getImportsResponseValidator
} from './books_validation'
const exportBookApiUrl = '/exports'
const importBookApiUrl = '/imports'

let exportTaskId: string
let importTaskId: string

interface ExportBookRequestBody {
  bookId: string,
  type: ExportBookType
}

interface ImportBookRequestBody {
  bookId: string,
  type: ImportBookType,
  url: string
}

let app = express()
app.use(bodyParser.json({ limit: '1mb' }))
app.use(booksAPI)

const createNewRequestBody = ({ bookId, type, url }: { bookId?: any, type?: any, url?: any }) => {
  const body = {}
  if (bookId) Object.assign(body, { bookId })
  if (type) Object.assign(body, { type })
  if (url) Object.assign(body, { url })
  return body
}

const validNewExportRequestBody = createNewRequestBody({
  bookId: 'ac2d64a0-c44e-11e9-b933-559a4f9d4e90',
  type: ExportBookType.pdf
}) as ExportBookRequestBody
it('[POST /exports] should response with created task for book export', async () => {
  const res = await request(app)
    .post(exportBookApiUrl)
    .send(validNewExportRequestBody)
    .expect(200)

  const body = res.body

  newExportResponseValidator(body)
  expect(newExportResponseValidator.errors).toBeNull()

  expect(body.export.state).toBe(TaskState.pending)
  expect(body.export.bookId).toBe(validNewExportRequestBody.bookId)
  expect(body.export.type).toBe(validNewExportRequestBody.type)

  exportTaskId = body.export.id
})

it('[POST /exports] should response with 400 if request is invalid', async () => {
  const reqs = [
    createNewRequestBody({ bookId: 1223456, type: ExportBookType.pdf }),
    createNewRequestBody({ type: ExportBookType.pdf }),
    createNewRequestBody({ bookId: 'ac2d64a0-c44e-11e9-b933-559a4f9d4e90' }),
    createNewRequestBody({ type: '#FF0000' })
  ]

  for (let reqBody of reqs) {
    const res = await request(app)
      .post(exportBookApiUrl)
      .send(reqBody)
      .expect(400)
  }
})

it('[GET /exports] should responsee with pending and finished exports', async () => {
  const res = await request(app).get(exportBookApiUrl).expect(200)
  const body = res.body

  getExportsResponseValidator(res.body)
  expect(getExportsResponseValidator.errors).toBeNull()

  const task = body.pending[exportTaskId]
  expect(task.bookId).toBe(validNewExportRequestBody.bookId)
})

const validNewImportRequestBody = createNewRequestBody({
  bookId: 'ac2d64a0-c44e-11e9-b933-559a4f9d4e9',
  type: ImportBookType.wattpad,
  url: 'https://www.adobe.com/be_en/active-use/pdf/Alice_in_Wonderland.pdf'
}) as ImportBookRequestBody
it('[POST /imports] should response with created task for book import', async () => {
  const res = await request(app)
    .post(importBookApiUrl)
    .send(validNewImportRequestBody)
    .expect(200)

  const body = res.body

  newImportResponseValidator(body)
  expect(newImportResponseValidator.errors).toBeNull()

  expect(body.import.state).toBe(TaskState.pending)
  expect(body.import.bookId).toBe(validNewImportRequestBody.bookId)
  expect(body.import.type).toBe(validNewImportRequestBody.type)

  importTaskId = body.import.id
})

it('[POST /imports] should response with 400 if request is invalid', async () => {
  const reqs = [
    createNewRequestBody({ bookId: 1223456, type: ImportBookType.pdf, url: validNewImportRequestBody.url }),
    createNewRequestBody({ type: ImportBookType.pdf, url: validNewImportRequestBody.url }),
    createNewRequestBody({ bookId: 'ac2d64a0-c44e-11e9-b933-559a4f9d4e90', url: validNewImportRequestBody.url }),
    createNewRequestBody({ type: '#FF0000', url: validNewImportRequestBody.url, bookId: validNewImportRequestBody.bookId }),
    createNewRequestBody({ type: validNewImportRequestBody.type, bookId: validNewImportRequestBody.bookId })
  ]

  for (let reqBody of reqs) {
    const res = await request(app)
      .post(importBookApiUrl)
      .send(reqBody)
      .expect(400)
  }
})

it('[GET /imports should responsee with pending and finished imports', async () => {
  const res = await request(app).get(importBookApiUrl).expect(200)
  const body = res.body

  getImportsResponseValidator(res.body)
  expect(getImportsResponseValidator.errors).toBeNull()

  const task = body.pending[importTaskId]
  expect(task.bookId).toBe(validNewImportRequestBody.bookId)
})
