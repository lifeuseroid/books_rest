import express, { Request, Response } from 'express'
import { ajv, ajvHelpers } from '../schema'
import { exportBook, getExportsTasks } from '../../books/export'
import { importBook, getImportTasks } from '../../books/import'
import { ImportBookType, ExportBookType } from '../../types'
const router = express.Router()

const newExportValidator = ajv.compile({
  type: 'object',
  properties: {
    bookId: { type: 'string' },
    type: { type: 'string', enum: Object.values(ExportBookType) }
  },
  required: ['bookId', 'type']
})
router.post('/exports', function (req: Request, res: Response) {
  ajvHelpers.validate(newExportValidator, req.body)
  const { bookId, type } = req.body
  const exportsTask = exportBook(bookId, type)
  res.send({ export: exportsTask })
})

// GET books/exports/
router.get('/exports', (req: Request, res: Response) => {
  const exportsTasks = getExportsTasks()
  res.send(exportsTasks)
})

// POST books/imports/
const newImportValidator = ajv.compile({
  type: 'object',
  properties: {
    bookId: { type: 'string' },
    type: { type: 'string', enum: Object.values(ImportBookType) },
    url:  { type: 'string' }
  },
  required: ['bookId', 'type', 'url']
})
router.post('/imports', (req: Request, res: Response) => {
  ajvHelpers.validate(newImportValidator, req.body)
  const { bookId, type, url } = req.body
  const importTask = importBook(bookId, type, url)
  res.send({ import: importTask })
})

// GET books/imports
router.get('/imports', (req: Request, res: Response) => {
  const importTasks = getImportTasks()
  res.send(importTasks)
})

export default router
