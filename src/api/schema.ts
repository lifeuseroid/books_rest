import Ajv, { ValidateFunction, SchemaValidateFunction } from 'ajv'
import { InvalidFieldError } from '../errors'
const ajv = Ajv({ allErrors: true, removeAdditional: 'all' })

const ajvHelpers = {
  validate: (validator: ValidateFunction, object: object) => {
    if (!validator(object) && validator.errors) {
      throw new InvalidFieldError(validator.errors)
    }
  }
}

export { ajv, ajvHelpers }
