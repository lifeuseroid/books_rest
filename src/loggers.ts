import pino from 'pino'

const makeLogger = (name: string) => pino({ name, serializers: { err: pino.stdSerializers.err } })

const systemLogger = makeLogger('systemLogger')
const expressLogger = makeLogger('expressLogger')

export { makeLogger, systemLogger, expressLogger }
