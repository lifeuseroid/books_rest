import dotenv from 'dotenv'
if (!dotenv.config().parsed) {
  console.error('FAILED ON .env READING ATTEMPT')
  process.exit()
}

import express, { Request, Response, NextFunction } from 'express'
import { expressLogger, systemLogger } from './loggers'
import bodyParser from 'body-parser'
import expressPinoLogger from 'express-pino-logger'
import { CustomError, InternalError } from './errors'
import api from './api'

const app = express()
app.use(bodyParser.json({ limit: '1mb' }))
app.use(expressPinoLogger({ logger: expressLogger }))
app.use('/api', api)
app.use((err: CustomError, req: Request, res: Response, next: NextFunction) => {
  const { status } = err
  if (status) {
    res.status(status).send({ err })
  } else {
    systemLogger.error(err)
    const internalError = new InternalError()
    res.status(internalError.status).send(internalError)
  }
})

export default app
