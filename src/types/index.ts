export enum ExportBookType {
  epub = 'epub',
  pdf = 'pdf'
}

export enum ImportBookType {
  word = 'word',
  pdf = 'pdf',
  wattpad = 'wattpad',
  evernote = 'evernote'
}

export enum TaskState {
  pending = 'pending',
  finished = 'finished'
}

export enum TaskProcessingTime {
  epub = 10 * 1000,
  pdf = 25 * 1000,
  import = 60 * 1000
}

export interface Task {
  id: string,
  state: TaskState,
  created_at: number,
  updated_at?: number,
  processingTime?: number
}
